function getMaxSumSubset(arr) {
  let maxSum = -Infinity;
  let currentSum = 0;
  let startIndex = 0;
  let bestStartIndex = 0;
  let bestEndIndex = 0;

  for (let i = 0; i < arr.length; i++) {
    if (currentSum <= 0) {
      startIndex = i;
      currentSum = arr[i];
    } else {
      currentSum += arr[i];
    }

    if (currentSum > maxSum) {
      maxSum = currentSum;
      bestStartIndex = startIndex;
      bestEndIndex = i;
    }
  }

  return [bestStartIndex, bestEndIndex];
}

module.exports = getMaxSumSubset;
