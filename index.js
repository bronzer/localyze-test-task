const getMaxSumSubset = require('./max-sum-subset');
const getMostFrequentSymbol = require('./most-frequent-symbol');
const getLastWordLength = require('./last-word-length');

console.log('Most frequent symbol:');
console.log(getMostFrequentSymbol('Suuuuure'));
console.log(getMostFrequentSymbol('asdjhasdsdaaabds'));
console.log(getMostFrequentSymbol('Boat'));
console.log(getMostFrequentSymbol('A hot  dog'));
console.log('----------------------');

console.log('Maximum subset sum results:')
console.log(getMaxSumSubset([-3, 3, 4, -5, 5, 7, -10, 9]));
console.log(getMaxSumSubset([2, -4, 6, 8, -10, 100, -6, 5]));
console.log(getMaxSumSubset([-3, -2, -5]));
console.log(getMaxSumSubset([3, 2, 5]));
console.log('----------------------');

console.log('Last word length:');
console.log(getLastWordLength('asd asdqa asdqa1        '));
console.log(getLastWordLength('Hello World'));
console.log(getLastWordLength(' fly me   to   the moon  '));
console.log(getLastWordLength('luffy is still joyboy'));
