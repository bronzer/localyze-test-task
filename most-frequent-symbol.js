function getMostFrequentSymbol(str) {
  const dictionary = {};
  let mostFrequentSymbol = '';
  let mostFrequentSymbolLength = 0;

  for (let i = 0; i < str.length; i++) {
    dictionary[str[i]] = (dictionary[str[i]] ? (dictionary[str[i]] + 1) : 1);

    if (dictionary[str[i]] > mostFrequentSymbolLength) {
      mostFrequentSymbolLength = dictionary[str[i]];
      mostFrequentSymbol = str[i];
    }
  }

  return mostFrequentSymbol;
}

module.exports = getMostFrequentSymbol;
