function getLastWordLength(str) {
  let lastWordLength = 0;

  for (let i = str.length - 1; i >= 0; i--) {
    if (str[i] !== ' ') {
      lastWordLength += 1;
    } else if (lastWordLength) {
      break;
    }
  }

  return lastWordLength;
}

module.exports = getLastWordLength;
